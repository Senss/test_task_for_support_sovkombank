-- TASK 1 --
SELECT Id, DISTINCT NameProduct as Наименование продукта
FROM Products
ORDER BY NameProduct;

-- TASK 2 --
SELECT
    e.Id,
    e.SurName AS 'Фамилия',
    e.FirstName AS 'Имя',
    e.MiddleName AS 'Отчество',
    e.DateBirth AS 'Дата рождения',
    CASE
        WHEN p.Position IS NULL
        THEN 'Не указано'
        ELSE p.Position
        END AS 'Должность'
FROM Employees e
LEFT JOIN Positions p ON
    e.Id = p.Id;

-- TASK 3 --
SELECT
    Date,
    MAX(SumDoc) AS 'Максимальная сумма',
    MIN(SumDoc) AS 'Минимальная сумма',
    AVG(SumDoc) AS 'Средняя сумма'
FROM Sums
GROUP BY Date;


-- TASK 4 --
-- Если повторения порядкового номера не допустимы --
SELECT
    ROW_NUMBER() OVER (ORDER BY Score DESC) AS 'NUMBER',
    FIO,
    Score
FROM Clients_Score
ORDER BY Score DESC;

-- Если повторения порядкового номера допустимы --
-- Дублирует значение номера и пропускает следующее значение --
SELECT
    RANK() OVER (ORDER BY Score DESC) AS 'NUMBER',
    FIO,
    Score
FROM Clients_Score
ORDER BY Score DESC;

-- Если нужны все ранговые значения без пропусков--
SELECT
    DENSE_RANK() OVER (ORDER BY Score DESC) AS 'NUMBER',
    FIO,
    Score
FROM Clients_Score
ORDER BY Score DESC;

-- TASK 5 --
-- Ситуация с повторениями такая же как и в 4, при необходимости можно использовать RANK() и DENSE_RANK()
SELECT
    ROW_NUMBER() OVER (PARTITION BY Department ORDER BY Salary) AS 'NUMBER',
    Department,
    FIO,
    Salary
FROM Employee_Salary
ORDER BY Department;